/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef ODOM_DATA_PROCESSOR_HPP_
#define ODOM_DATA_PROCESSOR_HPP_
#include "laser_odom_calibrator/calib_data.hpp"
#include <vector>
namespace qrb_ros
{
namespace laser_odom_calibrator
{
class OdomDataProcessor
{
private:
    /* data */
public:
    OdomDataProcessor(/* args */);
    ~OdomDataProcessor();
    void frame2frame_pose_estimation(std::vector<qrb_ros::laser_odom_calibrator::Odom_Data>& odom_datas);
};
}
}
#endif