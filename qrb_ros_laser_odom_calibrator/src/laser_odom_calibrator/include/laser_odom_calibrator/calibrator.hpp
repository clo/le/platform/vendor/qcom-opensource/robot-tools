/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef CALIBRATOR_HPP_
#define CALIBRATOR_HPP_
#include "laser_odom_calibrator/laser_pose_estimator.hpp"
#include "laser_odom_calibrator/odom_data_processor.hpp"
#include "laser_odom_calibrator/parameters_io.hpp"
#include "laser_odom_calibrator/solver.hpp"
#include "laser_odom_calibrator/calib_data.hpp"
namespace qrb_ros
{
namespace laser_odom_calibrator
{
class Calibrator
{
private:
    std::shared_ptr<qrb_ros::laser_odom_calibrator::ParametersIO> parameters_io_ = nullptr;
    std::shared_ptr<qrb_ros::laser_odom_calibrator::LaserPoseEstimator> laser_pose_estimator_ = nullptr;
    std::shared_ptr<qrb_ros::laser_odom_calibrator::OdomDataProcessor> odom_data_processor_ = nullptr;
    std::shared_ptr<qrb_ros::laser_odom_calibrator::Slover> extrinsic_solver_ = nullptr;
    std::vector<qrb_ros::laser_odom_calibrator::Laser_Data> laser_data_set_;
    std::vector<qrb_ros::laser_odom_calibrator::Odom_Data> odom_data_set_;
    bool initialize();
public:
    Calibrator(const std::vector<Laser_Data>& laser_data_set, const std::vector<Odom_Data>& odom_data_set);
    bool calibrate();
};
}
}
#endif