/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef LASER_POSE_ESTIMATOR_HPP_
#define LASER_POSE_ESTIMATOR_HPP_
#include <pcl/point_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/common/common_headers.h>
#include <pcl/PCLHeader.h>
//#include <pcl/visualization/pcl_visualizer.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/search/kdtree.h>
#include <pcl/registration/icp.h>
#include <pcl/io/pcd_io.h>
#include <pcl/registration/transformation_estimation_2D.h>
#include <pcl/registration/transformation_estimation_lm.h>
#include <pcl/registration/transformation_estimation.h>
#include <pcl/registration/icp_nl.h>
#include <eigen3/Eigen/Core>
#include "laser_odom_calibrator/calib_data.hpp"
#include <vector>
#include <queue>
namespace qrb_ros
{
namespace laser_odom_calibrator
{
class LaserPoseEstimator
{
private:
    double long_length_ = 1.2;
    double short_length_ = 0.5;
    double max_dist_seen_as_continuous_ = 0.07;
    double line_length_tolerance_ = 0.25;
    double ransac_fitline_dist_th_ = 0.04;
    int ransac_max_iterations_ = 10000;
    int min_point_num_ = 10;
    double min_proportion_ = 0.01;

    pcl::SACSegmentation<pcl::PointXYZ> seg_;
    pcl::ExtractIndices<pcl::PointXYZ> extractor_;
    void seg_all_lines(pcl::PointCloud<pcl::PointXYZ>& source_points,
                        std::vector<pcl::ModelCoefficients>& coefficients_set,
                        std::vector<pcl::PointCloud<pcl::PointXYZ>>& line_seg_set);

    void search_end_pts(const std::vector<pcl::PointCloud<pcl::PointXYZ>>& line_seg_set,
                        std::vector<std::pair<pcl::PointXYZ,pcl::PointXYZ>>& endpt_set);

    double dist(const pcl::PointXYZ& a, const pcl::PointXYZ& b);

    void clustering(pcl::PointCloud<pcl::PointXYZ>& point_cloud, std::vector<std::vector<int>>& cluster);

    void clustered_point_clouds(const pcl::PointCloud<pcl::PointXYZ>& point_cloud,
                                const std::vector<std::vector<int>>& cluster,
                                std::vector<pcl::PointCloud<pcl::PointXYZ>>& all_clustered_sets);

    void filter_using_length(const std::vector<std::pair<pcl::PointXYZ,pcl::PointXYZ>>& endpts,
                            int& long_edge_index, int& short_edge_index);

    void calculate_parameters(const pcl::ModelCoefficients& long_line_parameter,
                                const pcl::ModelCoefficients& short_line_parameter,
                              qrb_ros::laser_odom_calibrator::Laser_Data& laser_data);

    void normalize_parameters(const std::pair<pcl::PointXYZ,pcl::PointXYZ>& long_edge_endpts,
                            const std::pair<pcl::PointXYZ,pcl::PointXYZ>& short_edge_endpts,
                            qrb_ros::laser_odom_calibrator::Laser_Data& laser_data);

    void two_line_intersaction(const Eigen::Vector3d& line1, const Eigen::Vector3d& line2,
                            Eigen::Vector2d& intersaction_point);

    void frame2frame_pose_estimation(std::vector<qrb_ros::laser_odom_calibrator::Laser_Data>& laser_datas);
public:
    LaserPoseEstimator(const double& long_length,const double& short_length);
    ~LaserPoseEstimator();
    void set_laser_process_parameters(const double& max_dist_seen_as_continuous,
                                      const double& line_length_tolerance,
                                      const double& ransac_fitline_dist_th,
                                      const int& ransac_max_iterations,
                                      const int& min_point_num_stop_ransac,
                                      const double& min_proportion_stop_ransac);
    void process_laser_data(std::vector<qrb_ros::laser_odom_calibrator::Laser_Data>& input_data);
};
}
}

#endif