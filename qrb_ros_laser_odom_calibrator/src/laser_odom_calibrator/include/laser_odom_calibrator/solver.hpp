/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef SLOVER_HPP_
#define SLOVER_HPP_
#include <eigen3/Eigen/Core>
#include <Eigen/Dense>
#include "laser_odom_calibrator/calib_data.hpp"
#include <vector>
namespace qrb_ros
{
namespace laser_odom_calibrator
{
class Slover
{
private:
    Eigen::Vector2d extrinsic_xy_;//from lidar frame to odom frames
    Eigen::Matrix2d extrinsic_yaw_;//from lidar frame to odom frames
    double extrinsic_yaw_angle_;
    double diff_tolerance_laser_odom_ = 0.05;
    bool solve_linear_equation(std::vector<qrb_ros::laser_odom_calibrator::Odom_Data>& odom_datas,
                                 std::vector<qrb_ros::laser_odom_calibrator::Laser_Data>& laser_datas);
    void solve_linear_equation2(std::vector<qrb_ros::laser_odom_calibrator::Odom_Data>& odom_datas,
                                 std::vector<qrb_ros::laser_odom_calibrator::Laser_Data>& laser_datas);

    void rotation2quaternion(const Eigen::Matrix2d& rotation,double* quaternion);
    void quaternion2rotation(const double* quaternion, Eigen::Matrix2d& rotation);
public:
    bool calibrate(std::vector<qrb_ros::laser_odom_calibrator::Odom_Data>& odom_datas,
                    std::vector<qrb_ros::laser_odom_calibrator::Laser_Data>& laser_datas);
    void set_tolerance(const double& diff_tolerance_laser_odom);
    void print_extrinsic_ladar2odom();
    void get_extrinsic(Eigen::Matrix2d& rotation,Eigen::Vector2d& translation);
    Slover();
    ~Slover();
};
}
}
#endif