/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "laser_odom_calibrator/solver.hpp"
qrb_ros::laser_odom_calibrator::Slover::Slover(/* args */)
{
}

qrb_ros::laser_odom_calibrator::Slover::~Slover()
{
}
void qrb_ros::laser_odom_calibrator::Slover::set_tolerance(const double& diff_tolerance_laser_odom)
{
    diff_tolerance_laser_odom_ = diff_tolerance_laser_odom;
}

bool qrb_ros::laser_odom_calibrator::Slover::solve_linear_equation(std::vector<qrb_ros::laser_odom_calibrator::Odom_Data>& odom_datas,
std::vector<qrb_ros::laser_odom_calibrator::Laser_Data>& laser_datas)
{
    int observe_num = 0;
    for(size_t i=0;i<laser_datas.size();++i){
        if(laser_datas[i].can_be_used == false){
            odom_datas[i].can_be_used = false;
        }
        if(std::isnan(laser_datas[i].last2current_xy.norm())){
            laser_datas[i].can_be_used = false;
            odom_datas[i].can_be_used = false;
        }
    }
    for(size_t i=1;i<odom_datas.size();++i){
        if(odom_datas[i].can_be_used == false || odom_datas[i-1].can_be_used == false){
            continue;
        }
        double laser_odom_diff = fabs(odom_datas[i].last2current_xy.norm() - laser_datas[i].last2current_xy.norm());
        if(laser_odom_diff > diff_tolerance_laser_odom_ ){
            std::cout<<"odom_datas[i].last2current_xy.norm() "<<odom_datas[i].last2current_xy.norm()<<std::endl;
            std::cout<<"laser_datas[i].last2current_xy.norm() "<<laser_datas[i].last2current_xy.norm()<<std::endl;
            std::cout<<"laser_odom_diff "<<laser_odom_diff<<std::endl;
            continue;
        }else{

            observe_num ++;
            odom_datas[i].can_be_used = true;
            laser_datas[i].can_be_used = true;
        }
    }
    if(observe_num < 3){
        std::cout<<"no enough valid data, calibration failed!"<<std::endl;
        return false;
    }
    std::cout<<"observe_num = "<<observe_num<<std::endl;
    Eigen::MatrixXd A(observe_num * 2, 4);
    Eigen::MatrixXd b(observe_num * 2, 1);
    int current_observe = 0;
    for(size_t i=1;i<odom_datas.size();++i){
        std::cout<<i<<" th odom movement norm = "<<odom_datas[i].last2current_xy.norm()<<std::endl;
        std::cout<<i<<" th laser movement norm = "<<laser_datas[i].last2current_xy.norm()<<std::endl;
        double laser_odom_diff = fabs(odom_datas[i].last2current_xy.norm() - laser_datas[i].last2current_xy.norm());
        if((!odom_datas[i].can_be_used)||(!laser_datas[i].can_be_used)){
            //std::cout<<"The diff of odom and laser = " <<laser_odom_diff <<std::endl;
            std::cout<<i <<" th data can't be used, discard this set of data"<<std::endl;
            continue;
        }

        if(laser_odom_diff > diff_tolerance_laser_odom_ ){
            std::cout<<"The diff of odom and laser = " <<laser_odom_diff <<std::endl;
            std::cout<<"discard this set of data"<<std::endl;
            continue;
        }
        Eigen::Matrix<double, 2, 4> A_temp;
        Eigen::Matrix<double, 2, 1> b_temp;
        Eigen::Matrix2d laser_rotation = laser_datas[i].last2current_rotation;
        Eigen::Vector2d laser_last2current_xy = laser_datas[i].last2current_xy;
        Eigen::Vector2d odom_last2current_xy = odom_datas[i].last2current_xy;
        A_temp << laser_rotation(0,0) - 1.0,  laser_rotation(0,1), -laser_last2current_xy(0), laser_last2current_xy(1),
                  laser_rotation(1,0), laser_rotation(1,1) - 1.0, -laser_last2current_xy(1),-laser_last2current_xy(0);
        b_temp << -odom_last2current_xy(0), -odom_last2current_xy(1);
        A.block<2,4>(2*current_observe,0) = A_temp;
        b.block<2,1>(2*current_observe,0) = b_temp;
        current_observe++;
    }
    Eigen::Vector4d x = (A.transpose() * A).ldlt().solve(A.transpose() * b);
    extrinsic_xy_ << x(0), x(1);
    double yaw_extrinsic = atan2(x(3),x(2));
    extrinsic_yaw_angle_ = yaw_extrinsic;
    extrinsic_yaw_ << cos(yaw_extrinsic), -sin(yaw_extrinsic), sin(yaw_extrinsic), cos(yaw_extrinsic);
    return true;
}
void qrb_ros::laser_odom_calibrator::Slover::solve_linear_equation2(std::vector<Odom_Data>& odom_datas, std::vector<Laser_Data>& laser_datas)
{
    int observe_num = 0;

    for(size_t i=1;i<odom_datas.size();++i){
        if(fabs(odom_datas[i].last2current_xy.norm() - laser_datas[i].last2current_xy.norm())>diff_tolerance_laser_odom_
            || std::isnan(laser_datas[i].last2current_xy.norm())){
            odom_datas[i].can_be_used = false;
            laser_datas[i].can_be_used = false;
        }else{
            observe_num ++;
            odom_datas[i].can_be_used = true;
            laser_datas[i].can_be_used = true;
        }
    }
    std::cout<<"observe_num = "<<observe_num<<std::endl;
    Eigen::MatrixXd A(observe_num * 2, 4);
    Eigen::MatrixXd b(observe_num * 2, 1);
    int current_observe = 0;
    for(size_t i=1;i<odom_datas.size();++i){
        std::cout<<i<<" th odom movement norm = "<<odom_datas[i].last2current_xy.norm()<<std::endl;
        std::cout<<i<<" th laser movement norm = "<<laser_datas[i].last2current_xy.norm()<<std::endl;
        if(!odom_datas[i].can_be_used||!laser_datas[i].can_be_used){
            std::cout<<"discard "<<i<<"th observation"<<std::endl;
            continue;
        }
        Eigen::Matrix<double, 2, 4> A_temp;
        Eigen::Matrix<double, 2, 1> b_temp;
        Eigen::Matrix2d odom_rotation = odom_datas[i].last2current_rotation;
        Eigen::Vector2d laser_last2current_xy = laser_datas[i].last2current_xy;
        Eigen::Vector2d odom_last2current_xy = odom_datas[i].last2current_xy;
        A_temp << odom_rotation(0,0) - 1.0,  odom_rotation(0,1), -laser_last2current_xy(0), laser_last2current_xy(1),
                  odom_rotation(1,0), odom_rotation(1,1) - 1.0, -laser_last2current_xy(1),-laser_last2current_xy(0);
        b_temp << -odom_last2current_xy(0), -odom_last2current_xy(1);
        A.block<2,4>(2*current_observe,0) = A_temp;
        b.block<2,1>(2*current_observe,0) = b_temp;
        current_observe++;
    }
    Eigen::Vector4d x = (A.transpose() * A).ldlt().solve(A.transpose() * b);
    extrinsic_xy_ << x(0), x(1);
    double yaw_extrinsic = atan2(x(3),x(2));
    extrinsic_yaw_angle_ = yaw_extrinsic;
    extrinsic_yaw_ << cos(yaw_extrinsic), -sin(yaw_extrinsic), sin(yaw_extrinsic), cos(yaw_extrinsic);
}
bool qrb_ros::laser_odom_calibrator::Slover::calibrate(std::vector<Odom_Data>& odom_datas,std::vector<Laser_Data>& laser_datas)
{
    bool calib = solve_linear_equation(odom_datas,laser_datas);
    if(calib == false){
        return calib;
    }else{
        std::cout<<"calibration done! "<<std::endl;
        print_extrinsic_ladar2odom();
        return calib;
    }
    //solve_linear_equation2(odom_datas,laser_datas);
    //print_extrinsic_ladar2odom();
}
void qrb_ros::laser_odom_calibrator::Slover::print_extrinsic_ladar2odom()
{
    std::cout<<"extrinsic laser to odom is :"<<std::endl;
    std::cout<<"extrinsic_xy: "<<"\n"<<extrinsic_xy_<<std::endl;
    std::cout<<"extrinsic yaw: "<<extrinsic_yaw_angle_ / 3.14 * 180<<std::endl;
}
void qrb_ros::laser_odom_calibrator::Slover::get_extrinsic(Eigen::Matrix2d& rotation,Eigen::Vector2d& translation)
{
    rotation = extrinsic_yaw_;
    translation = extrinsic_xy_;
}
void qrb_ros::laser_odom_calibrator::Slover::rotation2quaternion(const Eigen::Matrix2d& rotation,double* quaternion)
{
    Eigen::Matrix3d rot3d;
    rot3d << rotation(0,0), rotation(0,1), 0.0,
             rotation(1,0), rotation(1,1), 0.0,
                       0.0,             0, 1.0;
    Eigen::Quaterniond q(rot3d);
    quaternion[0] = q.w();
    quaternion[1] = q.x();
    quaternion[2] = q.y();
    quaternion[3] = q.z();
}
void qrb_ros::laser_odom_calibrator::Slover::quaternion2rotation(const double* quaternion, Eigen::Matrix2d& rotation)
{
    Eigen::Quaterniond q;
    q.w() = quaternion[0];
    q.x() = quaternion[1];
    q.y() = quaternion[2];
    q.z() = quaternion[3];
    Eigen::Matrix3d rotation3d;
    rotation3d = q.toRotationMatrix();
    rotation = rotation3d.block<2,2>(0,0);
}
