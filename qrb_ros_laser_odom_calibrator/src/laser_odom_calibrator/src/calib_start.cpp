/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "data_colletor/data_collector.hpp"
#include "laser_odom_calibrator/calibrator.hpp"
#include <iostream>
int main(int argc, char * argv[])
{
    rclcpp::init(argc, argv);
    auto capture_data_node = std::make_shared<qrb_ros::laser_odom_collector::DataCollector>();
    std::cout<<"Start data collection: \n"<<
    "Press 'v' and Enter to record current data.Press 'b' and Enter to stop data collecting"<<std::endl;
    std::string keyboard_input;
    while(std::cin>>keyboard_input){
        if(keyboard_input.empty()){
            continue;
        }
        else if(keyboard_input=="v"){
            capture_data_node->start_capture();
            while (!capture_data_node->current_capture_succeed()){
                rclcpp::spin_some(capture_data_node);
            }
        }
        else if(keyboard_input=="b"){
            break;
        }else{
            continue;
        }
    }
    auto calibrator = std::make_shared<qrb_ros::laser_odom_calibrator::Calibrator>(capture_data_node->laser_data_set,capture_data_node->odom_data_set);
    calibrator->calibrate();
    return 0;
}