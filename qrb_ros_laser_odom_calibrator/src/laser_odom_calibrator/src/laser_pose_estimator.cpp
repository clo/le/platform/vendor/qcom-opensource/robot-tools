/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "laser_odom_calibrator/laser_pose_estimator.hpp"
qrb_ros::laser_odom_calibrator::LaserPoseEstimator::LaserPoseEstimator(const double& long_length,const double& short_length)
:long_length_(long_length),short_length_(short_length)
{
    std::cout<<"start laser process "<<std::endl;
}

qrb_ros::laser_odom_calibrator::LaserPoseEstimator::~LaserPoseEstimator()
{
}

void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::set_laser_process_parameters(const double& max_dist_seen_as_continuous,
const double& line_length_tolerance, const double& ransac_fitline_dist_th, const int& ransac_max_iterations,
const int& min_point_num_stop_ransac, const double& min_proportion_stop_ransac)
{
    max_dist_seen_as_continuous_ = max_dist_seen_as_continuous;
    line_length_tolerance_ = line_length_tolerance;
    ransac_fitline_dist_th_ = ransac_fitline_dist_th;
    ransac_max_iterations_ = ransac_max_iterations;
    min_point_num_ = min_point_num_stop_ransac;
    min_proportion_ = min_proportion_stop_ransac;
    seg_.setOptimizeCoefficients(true);
    seg_.setModelType(pcl::SACMODEL_LINE);
    seg_.setMethodType(pcl::SAC_RANSAC);
    seg_.setMaxIterations(ransac_max_iterations_);
    seg_.setDistanceThreshold(ransac_fitline_dist_th_);
}

void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::seg_all_lines
(pcl::PointCloud<pcl::PointXYZ>& source_points,
std::vector<pcl::ModelCoefficients>& coefficients_set,
std::vector<pcl::PointCloud<pcl::PointXYZ>>& line_seg_set)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = source_points.makeShared();
    int nr_points = (int)cloud->size();
    while(cloud->size()>nr_points*min_proportion_ && cloud->size()>min_point_num_){
        pcl::PointIndices::Ptr inliers(new pcl::PointIndices);
        pcl::ModelCoefficients::Ptr coefficients(new pcl::ModelCoefficients);
        pcl::PointCloud<pcl::PointXYZ> cloud_line;
        seg_.setInputCloud(cloud);
        seg_.segment(*inliers,*coefficients);
        if(inliers->indices.size()==0){
            std::cout<<"no line detected !"<<std::endl;
            break;
        }
        extractor_.setInputCloud(cloud);
        extractor_.setIndices(inliers);
        extractor_.setNegative(false);
        extractor_.filter(cloud_line);
        line_seg_set.push_back(cloud_line);
        coefficients_set.push_back(*coefficients);
        extractor_.setNegative(true);
        extractor_.filter(*cloud);
    }
}
void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::search_end_pts
(const std::vector<pcl::PointCloud<pcl::PointXYZ>>& line_seg_set,
std::vector<std::pair<pcl::PointXYZ,pcl::PointXYZ>>& endpt_set)
{
    for(size_t i=0;i<line_seg_set.size();++i){
        pcl::PointXYZ anchor_point = line_seg_set[i][0];
        double max_dist = 0.0;
        pcl::PointXYZ endpt1;
        for(size_t j=0;j<line_seg_set[i].size();++j){
            double current_dist = dist(anchor_point,line_seg_set[i][j]);
            if(current_dist>max_dist){
                max_dist = current_dist;
                endpt1 = line_seg_set[i][j];
            }
        }
        anchor_point = endpt1;
        max_dist = 0.0;
        pcl::PointXYZ endpt2;
        for(size_t j=0;j<line_seg_set[i].size();++j){
            double current_dist = dist(anchor_point,line_seg_set[i][j]);
            if(current_dist>max_dist){
                max_dist = current_dist;
                endpt2 = line_seg_set[i][j];
            }
        }
        endpt_set.push_back(std::make_pair(endpt1,endpt2));
    }
}

void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::process_laser_data
(std::vector<Laser_Data>& input_data)
{
    //std::cout<<"input_data size =="<<input_data.size()<<std::endl;
    std::vector<std::vector<pcl::ModelCoefficients>> coefficients_sets;
    std::vector<std::vector<pcl::PointCloud<pcl::PointXYZ>>> line_seg_sets;
    std::vector<std::vector<std::pair<pcl::PointXYZ,pcl::PointXYZ>>> end_points_sets;
    for(size_t i=0;i<input_data.size();++i){
        //std::cout<<"enter loop"<<std::endl;
        std::vector<std::vector<int>> clusters;
/*         for(int j=0;j<input_data[i].point_cloud.size();j++){
            std::cout<<input_data[i].point_cloud[i].x<<" "<<
        } */
        clustering(input_data[i].point_cloud,clusters);
        //std::cout<<"clusters size =="<<clusters.size()<<std::endl;

        std::vector<pcl::PointCloud<pcl::PointXYZ>> all_clustered_sets;
        clustered_point_clouds(input_data[i].point_cloud,clusters,all_clustered_sets);
        std::vector<pcl::ModelCoefficients> coefficients_set;
        std::vector<pcl::PointCloud<pcl::PointXYZ>> line_seg_set;

        for(size_t j=0;j<all_clustered_sets.size();++j){
            seg_all_lines(all_clustered_sets[j],coefficients_set,line_seg_set);
        }
        std::vector<std::pair<pcl::PointXYZ,pcl::PointXYZ>> end_points_set;
        if(line_seg_set.size()==0){
            coefficients_sets.push_back(coefficients_set);
            line_seg_sets.push_back(line_seg_set);
            end_points_sets.push_back(end_points_set);
            input_data[i].can_be_used = false;
            continue;
        }
        //std::cout<<line_seg_set.size()<<" lines detected in "<< i<<" th laser data"<<std::endl;
/*         pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("ALL LINES"));
        viewer->addPointCloud<pcl::PointXYZ>(input_data[i].point_cloud.makeShared(),std::to_string(i)); */
        search_end_pts(line_seg_set,end_points_set);
        coefficients_sets.push_back(coefficients_set);
        line_seg_sets.push_back(line_seg_set);
        end_points_sets.push_back(end_points_set);
/*         for(size_t k=0;k<line_seg_set.size();++k){
            viewer->addArrow(end_points_set[k].first,end_points_set[k].second,0,1.0,0,std::to_string(k)+" th line");
        }
        viewer->spin(); */
    }
    std::vector<int> long_edge_index_set;
    std::vector<int> short_edge_index_set;
    for(size_t i=0;i<input_data.size();++i){
        std::cout<<"process "<<i<<" th laser data"<<std::endl;
        if(input_data[i].can_be_used == false){
            continue;
        }
        int long_edge_index;
        int short_edge_index;
        filter_using_length(end_points_sets[i],long_edge_index,short_edge_index);
        if(long_edge_index == -1 || short_edge_index == -1){
            std::cout<<i<<" th data no line length satisfied! "<<std::endl;
            long_edge_index_set.push_back(long_edge_index);
            short_edge_index_set.push_back(short_edge_index);
            input_data[i].can_be_used = false;
        }else{
            long_edge_index_set.push_back(long_edge_index);
            short_edge_index_set.push_back(short_edge_index);
        }

/*         pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("SPCIFIC LINES"));
        viewer->addPointCloud<pcl::PointXYZ>(input_data[i].point_cloud.makeShared(),std::to_string(i));
        viewer->addArrow(end_points_sets[i][long_edge_index].first,end_points_sets[i][long_edge_index].second,0,1.0,0,std::to_string(i)+" th long line");
        viewer->addArrow(end_points_sets[i][short_edge_index].first,end_points_sets[i][short_edge_index].second,1.0,0.0,0,std::to_string(i)+" th short line");
        viewer->spin(); */
    }
/*     for(size_t i=0;i<end_points_sets[0].size();++i){
        viewer->addArrow(end_points_sets[0][i].first,end_points_sets[0][i].second,0,1.0,0,std::to_string(i)+" line");
    } */
    for(size_t i=0;i<input_data.size();++i){
        if(input_data[i].can_be_used == false){
            continue;
        }
        calculate_parameters(coefficients_sets[i][long_edge_index_set[i]],coefficients_sets[i][short_edge_index_set[i]],input_data[i]);
        normalize_parameters(end_points_sets[i][long_edge_index_set[i]],end_points_sets[i][short_edge_index_set[i]],input_data[i]);
        //pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer("PCL Viewer"));
        //viewer->addPointCloud<pcl::PointXYZ>(input_data[i].point_cloud.makeShared(),std::to_string(i));
        /*pcl::PointXYZ intersaction;
        intersaction.x = input_data[i].intersaction_point(0);
        intersaction.y = input_data[i].intersaction_point(1);
        intersaction.z = 0;

        viewer->addArrow(intersaction,input_data[i].long_end_pt,0,1.0,0,std::to_string(i)+" long line");
        viewer->addArrow(intersaction,input_data[i].short_end_pt,1.0,0.0,0,std::to_string(i)+" short line");
        viewer->spin(); */
    }
    frame2frame_pose_estimation(input_data);

    //system("pause");
}

void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::calculate_parameters
(const pcl::ModelCoefficients& long_line_parameter,
const pcl::ModelCoefficients& short_line_parameter,
Laser_Data& laser_data)
{
    // cos(theta) = values[3] , sin(theta) = values[4]
    Eigen::Vector2d long_line_dir;
    Eigen::Vector2d short_line_dir;
    long_line_dir << long_line_parameter.values[3],long_line_parameter.values[4];
    short_line_dir << short_line_parameter.values[3],short_line_parameter.values[4];
    double b_short = short_line_parameter.values[1]-(short_line_dir[1]/short_line_dir[0])*short_line_parameter.values[0];
    double b_long = long_line_parameter.values[1]-(long_line_dir[1]/long_line_dir[0])*long_line_parameter.values[0];
    Eigen::Vector3d long_line; // ax+by+c = 0
    Eigen::Vector3d short_line;
    long_line << long_line_dir[1],-long_line_dir[0],long_line_dir[0]*b_long;
    short_line << short_line_dir[1],-short_line_dir[0],short_line_dir[0]*b_short;
    Eigen::Vector2d intersection_point;
    two_line_intersaction(long_line,short_line,intersection_point);
    laser_data.long_edge_direction = long_line_dir/long_line_dir.norm();
    laser_data.short_edge_direction = short_line_dir/short_line_dir.norm();
    laser_data.intersaction_point = intersection_point;
}
void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::normalize_parameters
(const std::pair<pcl::PointXYZ,pcl::PointXYZ>& long_edge_endpts,
const std::pair<pcl::PointXYZ,pcl::PointXYZ>& short_edge_endpts,
Laser_Data& laser_data)
{
    pcl::PointXYZ intersaction_point;
    intersaction_point.x = laser_data.intersaction_point[0];
    intersaction_point.y = laser_data.intersaction_point[1];
    intersaction_point.z = 0.0;
    pcl::PointXYZ long_edge_endpt1 = long_edge_endpts.first;
    pcl::PointXYZ long_edge_endpt2 = long_edge_endpts.second;
    pcl::PointXYZ vector1;

    if(dist(long_edge_endpt1,intersaction_point)>dist(long_edge_endpt2,intersaction_point)){
        vector1.x = long_edge_endpt1.x - intersaction_point.x;
        vector1.y = long_edge_endpt1.y - intersaction_point.y;
        vector1.z = 0.0;
        laser_data.long_end_pt = long_edge_endpt1;
    }else{
        vector1.x = long_edge_endpt2.x - intersaction_point.x;
        vector1.y = long_edge_endpt2.y - intersaction_point.y;
        vector1.z = 0.0;
        laser_data.long_end_pt = long_edge_endpt2;
    }
    if(vector1.x *laser_data.long_edge_direction[0]+vector1.y *laser_data.long_edge_direction[1]<0){
        laser_data.long_edge_direction *= -1.0;
    }
    pcl::PointXYZ short_edge_endpt1 = short_edge_endpts.first;
    pcl::PointXYZ short_edge_endpt2 = short_edge_endpts.second;
    pcl::PointXYZ vector2;

    if(dist(short_edge_endpt1,intersaction_point)>dist(short_edge_endpt2,intersaction_point)){
        vector2.x = short_edge_endpt1.x - intersaction_point.x;
        vector2.y = short_edge_endpt1.y - intersaction_point.y;
        vector2.z = 0.0;
        laser_data.short_end_pt = short_edge_endpt1;
    }else{
        vector2.x = short_edge_endpt2.x - intersaction_point.x;
        vector2.y = short_edge_endpt2.y - intersaction_point.y;
        vector2.z = 0.0;
        laser_data.short_end_pt = short_edge_endpt2;
    }
    if(vector2.x *laser_data.short_edge_direction[0]+vector2.y *laser_data.short_edge_direction[1]<0){
        laser_data.short_edge_direction *= -1.0;
    }
}
void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::clustering
(pcl::PointCloud<pcl::PointXYZ>& point_cloud, std::vector<std::vector<int>>& clusters)
{
    pcl::PointCloud<pcl::PointXYZ>::Ptr cloud = point_cloud.makeShared();
    pcl::KdTreeFLANN<pcl::PointXYZ> kdtree;
    kdtree.setInputCloud(cloud);
    std::vector<bool> processed(cloud->size(),false);
    for(size_t i=0;i<cloud->size();++i){
        if(processed[i]){
            continue;
        }
        std::vector<int> cluster;
        std::queue<int> queue;
        queue.push(i);
        while(!queue.empty()){
            int idx = queue.front();
            queue.pop();
            if(processed[idx]){
                continue;
            }
            processed[idx] = true;
            cluster.push_back(idx);
            std::vector<int> indices;
            std::vector<float> distances;
            if(kdtree.radiusSearch(cloud->points[idx],max_dist_seen_as_continuous_,indices,distances)>0){
                for(int id : indices){
                    if(!processed[id]){
                        queue.push(id);
                    }
                }
            }
        }
        if(!cluster.empty()){
            clusters.push_back(cluster);
        }
    }
}
void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::clustered_point_clouds
(const pcl::PointCloud<pcl::PointXYZ>& point_cloud,
const std::vector<std::vector<int>>& cluster,
std::vector<pcl::PointCloud<pcl::PointXYZ>>& all_clustered_sets)
{
    for(size_t i=0;i<cluster.size();++i){
        pcl::PointCloud<pcl::PointXYZ> clustered_set;
        for(size_t j=0;j<cluster[i].size();++j){
            clustered_set.push_back(point_cloud[cluster[i][j]]);
        }
        all_clustered_sets.push_back(clustered_set);
    }
}
void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::filter_using_length
(const std::vector<std::pair<pcl::PointXYZ,pcl::PointXYZ>>& endpts,
int& long_edge_index,
int& short_edge_index)
{
    std::vector<int> long_edge_indices;
    std::vector<int> short_edge_indices;
    for(size_t i=0;i<endpts.size();++i){
        double length = dist(endpts[i].first,endpts[i].second);
        //std::cout<<length<<"m line detected"<<std::endl;
        if(fabs(length-short_length_) <= line_length_tolerance_){
            short_edge_indices.push_back(i);
        }else if(fabs(length-long_length_) <= line_length_tolerance_){
            long_edge_indices.push_back(i);
        }
    }
    long_edge_index = -1;
    short_edge_index = -1;
    double min_dist = 0.3;
    for(size_t i=0;i<long_edge_indices.size();++i){
        for(size_t j=0;j<short_edge_indices.size();++j){
            pcl::PointXYZ long_edge_end_pt1 = endpts[long_edge_indices[i]].first;
            pcl::PointXYZ long_edge_end_pt2 = endpts[long_edge_indices[i]].second;
            pcl::PointXYZ short_edge_end_pt1 = endpts[short_edge_indices[j]].first;
            pcl::PointXYZ short_edge_end_pt2 = endpts[short_edge_indices[j]].second;
            double dist1 = dist(long_edge_end_pt1,short_edge_end_pt1);
            double dist2 = dist(long_edge_end_pt1,short_edge_end_pt2);
            double dist3 = dist(long_edge_end_pt2,short_edge_end_pt1);
            double dist4 = dist(long_edge_end_pt2,short_edge_end_pt2);
            std::vector<double> dists = {dist1,dist2,dist3,dist4};
            double local_min_dist = *std::min_element(dists.begin(),dists.end());
            if(local_min_dist<min_dist){
                min_dist = local_min_dist;
                long_edge_index = long_edge_indices[i];
                short_edge_index = short_edge_indices[j];
            }
        }
    }
}

double qrb_ros::laser_odom_calibrator::LaserPoseEstimator::dist
(const pcl::PointXYZ& a, const pcl::PointXYZ& b)
{
    return sqrt((a.x-b.x)*(a.x-b.x)+(a.y-b.y)*(a.y-b.y)+(a.z-b.z)*(a.z-b.z));
}
void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::two_line_intersaction
(const Eigen::Vector3d& line1,
const Eigen::Vector3d& line2,
Eigen::Vector2d& intersaction_point)
{
    Eigen::Matrix2d A;
    Eigen::Vector2d b;
    A<<line1[0],line1[1],line2[0],line2[1];
    b<<-line1[2],-line2[2];
    intersaction_point = A.inverse()*b;
}
void qrb_ros::laser_odom_calibrator::LaserPoseEstimator::frame2frame_pose_estimation
(std::vector<Laser_Data>& laser_datas)
{
    for(size_t i=1;i<laser_datas.size();++i){
        if(laser_datas[i].can_be_used == false){
            continue;
        }
        if(laser_datas[i-1].can_be_used == false){
            continue;
        }

        Eigen::Vector2d last_long_dir = laser_datas[i-1].long_edge_direction;
        Eigen::Vector2d last_short_dir = laser_datas[i-1].short_edge_direction;
        Eigen::Vector2d current_long_dir = laser_datas[i].long_edge_direction;
        Eigen::Vector2d current_short_dir = laser_datas[i].short_edge_direction;
        double norm_inv1 = 1.0/last_long_dir.dot(current_long_dir);
        double norm_inv2 = 1.0/last_short_dir.dot(current_short_dir);
        double cos_theta = (norm_inv1 + norm_inv2)/(norm_inv1*norm_inv1 + norm_inv2*norm_inv2);
        double last_yaw_ref = atan2(last_long_dir[1],last_long_dir[0]);
        double current_yaw_ref = atan2 (current_long_dir[1],current_long_dir[0]);
        double last_yaw_in_current_frame;
        last_yaw_in_current_frame = acos(fminl(fmaxl(cos_theta,-1.0),1.0));
        if(last_yaw_ref - current_yaw_ref > 0){//direction vector rotate clockwise
            //laser frame rotate is anti-clockwise
            last_yaw_in_current_frame *= -1.;
        }else{
            last_yaw_in_current_frame = last_yaw_in_current_frame;
        }
        laser_datas[i].last_yaw_in_current = last_yaw_in_current_frame;//laser frame rotate is inverse to direction vector
        //std::cout<<"last yaw in current: "<<laser_datas[i].last_yaw_in_current * 180 / 3.14<<std::endl;
        Eigen::Matrix2d rotation;
        rotation << cos(last_yaw_in_current_frame),-sin(last_yaw_in_current_frame),
                    sin(last_yaw_in_current_frame),cos(last_yaw_in_current_frame);
        laser_datas[i].last2current_rotation=rotation;
        Eigen::Vector2d translation;
        Eigen::Vector2d current_pt = laser_datas[i].intersaction_point;
        Eigen::Vector2d last_pt = laser_datas[i-1].intersaction_point;
        translation = current_pt - laser_datas[i].last2current_rotation*last_pt;
        //std::cout<<"last xy in current frame: \n"<<translation<<std::endl;
        laser_datas[i].last2current_xy = translation;
    }
}