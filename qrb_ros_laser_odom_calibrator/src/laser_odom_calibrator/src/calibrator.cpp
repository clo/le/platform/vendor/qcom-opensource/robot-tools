/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "laser_odom_calibrator/calibrator.hpp"
qrb_ros::laser_odom_calibrator::Calibrator::Calibrator(const std::vector<qrb_ros::laser_odom_calibrator::Laser_Data>& laser_data_set,
                                                 const std::vector<qrb_ros::laser_odom_calibrator::Odom_Data>& odom_data_set):
laser_data_set_(laser_data_set),odom_data_set_(odom_data_set)
{

}
bool qrb_ros::laser_odom_calibrator::Calibrator::initialize()
{
    parameters_io_ = std::make_shared<qrb_ros::laser_odom_calibrator::ParametersIO>();
    double long_edge_length;
    double short_edge_length;
    double max_dist_seen_as_continuous;
    double line_length_tolerance;
    double ransac_fitline_dist_th;
    int ransac_max_iterations;
    int min_point_num_stop_ransac;
    double min_proportion_stop_ransac;
    double diff_tolerance_laser_odom;
    bool initialize_done = parameters_io_ ->get_env_parameters(long_edge_length,short_edge_length);
    if(long_edge_length<0.0 || short_edge_length<0.0){
        std::cout<<"invalid parametes input "<<std::endl;
        return false;
    }
    if(long_edge_length < short_edge_length){
        std::cout<<"invalid parametes input "<<std::endl;
        return false;
    }
    initialize_done = initialize_done &&
                        parameters_io_ ->get_laser_process_parameters(max_dist_seen_as_continuous,
                                        line_length_tolerance,ransac_fitline_dist_th,
                                        ransac_max_iterations,min_point_num_stop_ransac,
                                        min_proportion_stop_ransac);
    initialize_done = initialize_done &&
                        parameters_io_->get_solver_parameters(diff_tolerance_laser_odom);
    if(!initialize_done){
        std::cout<<"Can't read parameters_input.ymal file, please check and re-calibrate"<<std::endl;
        return false;
    }
    laser_pose_estimator_ = std::make_shared<qrb_ros::laser_odom_calibrator::LaserPoseEstimator>
                            (long_edge_length,short_edge_length);
    laser_pose_estimator_->set_laser_process_parameters(max_dist_seen_as_continuous,line_length_tolerance,
                            ransac_fitline_dist_th,ransac_max_iterations,
                            min_point_num_stop_ransac,min_proportion_stop_ransac);
    odom_data_processor_ = std::make_shared<qrb_ros::laser_odom_calibrator::OdomDataProcessor>();
    extrinsic_solver_ = std::make_shared<qrb_ros::laser_odom_calibrator::Slover>();
    extrinsic_solver_->set_tolerance(diff_tolerance_laser_odom);
    return initialize_done;
}
bool qrb_ros::laser_odom_calibrator::Calibrator::calibrate()
{
    bool initialize_done = initialize();
    if(!initialize_done){
        return false;
    }
    if(laser_data_set_.size() != odom_data_set_.size()){
        std::cout<<"laser data can't match odometry data"<<std::endl;
        return false;
    }
    if(laser_data_set_.size()<3){
        std::cout<<"There is no enough data input, please re-capture data and re-calibrate \n"
                << "More than 10 sets of data input is recommended"<<std::endl;
        return false;
    }
    laser_pose_estimator_->process_laser_data(laser_data_set_);
    odom_data_processor_->frame2frame_pose_estimation(odom_data_set_);
    bool calib_success = extrinsic_solver_->calibrate(odom_data_set_,laser_data_set_);
    if(!calib_success){
        return false;
    }
    Eigen::Matrix2d rotation_l2o;
    Eigen::Vector2d translation_l2o;
    std::cout<<"record extrinsic parameters...."<<std::endl;
    extrinsic_solver_->get_extrinsic(rotation_l2o,translation_l2o);
    std::cout<<"save extrinsic parameters to extrinsic.yaml"<<std::endl;
    parameters_io_->save_extrinsic_parameters(rotation_l2o,translation_l2o);
    return true;
}