/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "laser_cam_calibra/calibrator_caller.hpp"
namespace qrb_ros {
namespace calibrator_caller {
CalibratorCaller::CalibratorCaller() {
  parameters_io_ = std::make_shared<ParametersIO>();
  std::string image_topic;
  std::string laser_topic;
  parameters_io_->get_topic_names(laser_topic, image_topic);
  capture_data_node_ =
      std::make_shared<DataCollector>(image_topic, laser_topic);
  calibrator_ = std::make_shared<qrb::laser_cam_calibrator::Calibrator>();
  qrb::laser_cam_calibrator::CameraInfo cam_info;
  qrb::laser_cam_calibrator::ChessboardInfo chessboard_info;
  qrb::laser_cam_calibrator::EnvParameters env_info;
  std::vector<std::string> laser_axis;
  parameters_io_->get_laser_axis(laser_axis);
  parameters_io_->get_cam_parameters(cam_info);
  parameters_io_->get_chessboard_parameters(chessboard_info);
  parameters_io_->get_laser_process_parameters(env_info);
  parameters_io_->get_relative_dist(relative_dist_);
  calibrator_->initialize(cam_info, chessboard_info, env_info);
  calibrator_->set_laser_axis_(laser_axis);
}
void CalibratorCaller::capture_laser_plane_image() {
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"),
              "Start laser plane image collection: \n");
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"),
              "Press 'v' and Enter to record current data.Press 'b' and Enter "
              "to stop data collecting");
  std::string keyboard_input;

  while (std::cin >> keyboard_input) {
    if (keyboard_input.empty()) {
      continue;
    } else if (keyboard_input == "v") {
      capture_data_node_->capture_laser_plane_image();
      while (!capture_data_node_->laser_plane_image_capture_succeed()) {
        rclcpp::spin_some(capture_data_node_);
      }
      if (calibrator_->find_chessboard(
              capture_data_node_->laser_plane_image.laser_plane_image)) {
        break;
      } else {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),
                    "Can not detect chessboard, please check input parameters "
                    "or press 'v' and enter to re-capture");
      }
    } else if (keyboard_input == "b") {
      break;
    } else {
      continue;
    }
  }
}
void CalibratorCaller::capture_data() {
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"), "Start data collection: \n");
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"),
              "Press 'v' and Enter to record current data.Press 'b' and Enter "
              "to stop data collecting");
  std::string keyboard_input;
  while (std::cin >> keyboard_input) {
    if (keyboard_input.empty()) {
      continue;
    } else if (keyboard_input == "v") {
      capture_data_node_->start_capture();
      while (!capture_data_node_->current_capture_succeed()) {
        rclcpp::spin_some(capture_data_node_);
      }
      if (!calibrator_->find_chessboard(
              capture_data_node_
                  ->camera_data_set[capture_data_node_->camera_data_set.size() -
                                    1]
                  .image)) {
        RCLCPP_INFO(rclcpp::get_logger("rclcpp"),
                    "Can not detect chessboard, please check input parameters "
                    "or press 'v' and enter to re-capture");
        capture_data_node_->camera_data_set.pop_back();
        capture_data_node_->laser_data_set.pop_back();
      }
    } else if (keyboard_input == "b") {
      break;
    } else {
      continue;
    }
  }
}
void CalibratorCaller::save_all_data() {
  cv::imwrite("laser_image.jpg",
              capture_data_node_->laser_plane_image.laser_plane_image);
  for (size_t i = 0; i < capture_data_node_->camera_data_set.size(); ++i) {
    std::string image_name = "image" + std::to_string(i) + ".jpg";
    std::string point_cloud_name = "point_cloud" + std::to_string(i) + ".pcd";
    cv::imwrite(image_name, capture_data_node_->camera_data_set[i].image);
    pcl::io::savePCDFile(point_cloud_name,
                         capture_data_node_->laser_data_set[i].point_cloud);
  }
  cv::FileStorage data_writer("saved_data.xml", cv::FileStorage::WRITE);
  data_writer.write("data_num",
                    int(capture_data_node_->camera_data_set.size()));
}
void CalibratorCaller::calibrate_with_saved_data() {
  cv::Mat laser_image;
  int data_num;
  laser_image = cv::imread("laser_image.jpg");
  qrb::laser_cam_calibrator::LaserPlane laser_plane_temp;
  laser_plane_temp.laser_plane_image = laser_image.clone();
  cv::FileStorage f_reader("saved_data.xml", cv::FileStorage::READ);
  data_num = f_reader["data_num"];
  f_reader.release();
  std::vector<qrb::laser_cam_calibrator::CameraData> camera_data_set_temp;
  std::vector<qrb::laser_cam_calibrator::LaserData> laser_data_set_temp;
  for (size_t i = 0; i < data_num; ++i) {
    qrb::laser_cam_calibrator::CameraData cam_data;
    qrb::laser_cam_calibrator::LaserData laser_data;
    cv::Mat image_temp = cv::imread("image" + std::to_string(i) + ".jpg");
    if (image_temp.empty() || !calibrator_->find_chessboard(image_temp)) {
      continue;
    }
    pcl::PointCloud<pcl::PointXYZ> point_cloud_temp;
    pcl::io::loadPCDFile("point_cloud" + std::to_string(i) + ".pcd",
                         point_cloud_temp);
    cam_data.image = image_temp.clone();
    laser_data.point_cloud = point_cloud_temp;
    camera_data_set_temp.push_back(cam_data);
    laser_data_set_temp.push_back(laser_data);
  }
  laser_plane_temp.dist_from_laser2chessboard_origin = relative_dist_;
  calibrator_->set_laser_plane(laser_plane_temp);
  calibrator_->input_data(camera_data_set_temp, laser_data_set_temp);
  bool done = calibrator_->calibrate();
  if(!done){
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"Calibration Failed!");
    return ;  
  }
  R_l2c = calibrator_->R_l2c;
  t_l2c = calibrator_->t_l2c * 1000;
  camera_data_set_temp = calibrator_->cam_data_set_;
  laser_data_set_temp = calibrator_->laser_data_set_;
  viz_test(camera_data_set_temp, laser_data_set_temp);
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"Calibration Done!");
}
void CalibratorCaller::calibrate() {
  capture_data_node_->laser_plane_image.dist_from_laser2chessboard_origin =
      relative_dist_;
  calibrator_->set_laser_plane(capture_data_node_->laser_plane_image);
  calibrator_->input_data(capture_data_node_->camera_data_set,
                          capture_data_node_->laser_data_set);
  save_all_data();
  bool done = calibrator_->calibrate();
  if(!done){
    RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"Calibration Failed!");
    return ;  
  }
  R_l2c = calibrator_->R_l2c;
  t_l2c = calibrator_->t_l2c * 1000;
  std::vector<qrb::laser_cam_calibrator::CameraData> camera_data_set_temp;
  std::vector<qrb::laser_cam_calibrator::LaserData> laser_data_set_temp;
  camera_data_set_temp = calibrator_->cam_data_set_;
  laser_data_set_temp = calibrator_->laser_data_set_;
  viz_test(camera_data_set_temp, laser_data_set_temp);
  parameters_io_->save_extrinsic_parameters(R_l2c,t_l2c);
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"),"Calibration Done!");
}
void CalibratorCaller::viz_test(
    std::vector<qrb::laser_cam_calibrator::CameraData> &cam_datas,
    std::vector<qrb::laser_cam_calibrator::LaserData> &laser_datas) {
  qrb::laser_cam_calibrator::CameraInfo cam_info;
  parameters_io_->get_cam_parameters(cam_info);
  for (size_t i = 0; i < cam_datas.size(); ++i) {
    pcl::PointCloud<pcl::PointXYZ> point_cloud = laser_datas[i].point_cloud;
    std::vector<cv::Point2f> pt_images;
    std::vector<cv::Point2f> pt_key;
    for (size_t j = 0; j < point_cloud.size(); ++j) {
      Eigen::Vector3d pt;
      pt << point_cloud[j].x, point_cloud[j].y, point_cloud[j].z;
      pt = pt * 1000;
      Eigen::Vector3d pt_cam;
      pt_cam = R_l2c * pt + t_l2c;
      if (pt_cam(2) < 0) {
        continue;
      }
      pt_cam = pt_cam / pt_cam(2);

      Eigen::Matrix3d intrinsic_matrix;
      cv::cv2eigen(cam_info.intrinsic, intrinsic_matrix);
      Eigen::Vector3d pt_img = intrinsic_matrix * pt_cam;
      cv::Point2f pt_image(pt_img(0), pt_img(1));
      pt_images.push_back(pt_image);
    }
    Eigen::Vector3d pt1;
    pt1 << laser_datas[i].pts_in_line[0](0), laser_datas[i].pts_in_line[0](1),
        0.0;
    pt1 = 1000 * pt1;
    Eigen::Vector3d pt2;
    pt2 << laser_datas[i].pts_in_line[1](0), laser_datas[i].pts_in_line[1](1),
        0.0;
    pt2 = 1000 * pt2;
    Eigen::Vector3d pt3;
    pt3 << laser_datas[i].pts_in_line[2](0), laser_datas[i].pts_in_line[2](1),
        0.0;
    pt3 = 1000 * pt3;
    Eigen::Vector3d pt1_cam;
    Eigen::Vector3d pt2_cam;
    Eigen::Vector3d pt3_cam;
    pt1_cam = R_l2c * pt1 + t_l2c;
    pt1_cam = pt1_cam / pt1_cam(2);
    pt2_cam = R_l2c * pt2 + t_l2c;
    pt2_cam = pt2_cam / pt2_cam(2);
    pt3_cam = R_l2c * pt3 + t_l2c;
    pt3_cam = pt3_cam / pt3_cam(2);
    Eigen::Matrix3d intrinsic_matrix;
    cv::cv2eigen(cam_info.intrinsic, intrinsic_matrix);
    Eigen::Vector3d pt1_img = intrinsic_matrix * pt1_cam;
    cv::Point2f pt1_image(pt1_img(0), pt1_img(1));
    pt_key.push_back(pt1_image);
    Eigen::Vector3d pt2_img = intrinsic_matrix * pt2_cam;
    cv::Point2f pt2_image(pt2_img(0), pt2_img(1));
    pt_key.push_back(pt2_image);
    Eigen::Vector3d pt3_img = intrinsic_matrix * pt3_cam;
    cv::Point2f pt3_image(pt3_img(0), pt3_img(1));
    pt_key.push_back(pt3_image);

    cv::Mat image_temp = cam_datas[i].image.clone();
    for (size_t j = 0; j < pt_images.size(); ++j) {
      cv::circle(image_temp, pt_images[j], 3, cv::Scalar(0, 0, 255), 2);
    }
    for (size_t j = 0; j < pt_key.size(); ++j) {
      cv::circle(image_temp, pt_key[j], 3, cv::Scalar(0, 255, 0), 2);
    }
    draw_line(image_temp, cam_datas[i].left_margin_line);
    draw_line(image_temp, cam_datas[i].right_margin_line);
    draw_line(image_temp, cam_datas[i].up_margin_line);
    draw_line(image_temp, cam_datas[i].down_margin_line);
    cv::imwrite("result" + std::to_string(i) + ".jpg", image_temp);
  }
}
void CalibratorCaller::draw_line(cv::Mat &image,
                                 const Eigen::Vector3d &image_line) {
  int width = image.cols;
  int height = image.rows;
  double a = image_line(0);
  double b = image_line(1);
  double c = image_line(2);
  cv::Point pt1, pt2;
  if (b != 0) {
    pt1 = cv::Point(0, -c / b);
    pt2 = cv::Point(width, -(a * width + c) / b);
  } else {
    pt1 = cv::Point(-c / a, 0);
    pt2 = cv::Point(-c / a, height);
  }
  cv::line(image, pt1, pt2, cv::Scalar(0, 255, 0), 2);
}

} // namespace calibrator_caller
} // namespace qrb_ros