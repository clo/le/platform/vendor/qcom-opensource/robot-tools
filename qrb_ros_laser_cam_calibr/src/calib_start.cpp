/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "laser_cam_calibra/calibrator_caller.hpp"
int main(int argc, char *argv[]) {
  rclcpp::init(argc, argv);
  auto calibrator_caller =
      std::make_shared<qrb_ros::calibrator_caller::CalibratorCaller>();
  RCLCPP_INFO(rclcpp::get_logger("rclcpp"),
              "Press 'v' and Enter to capture data and calibrate.Press 'b' and Enter "
              "to calibrate with saved data");
  std::string keyboard_input;
  std::cin >> keyboard_input;
  if("v" == keyboard_input){
    calibrator_caller->capture_laser_plane_image();
    calibrator_caller->capture_data();
    calibrator_caller->calibrate();
  }else if("b" == keyboard_input){
    calibrator_caller->calibrate_with_saved_data();
  }
  return 0;
}