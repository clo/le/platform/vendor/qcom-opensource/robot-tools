/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef DATA_COLLECTOR_HPP_
#define DATA_COLLECTOR_HPP_
#include "cv_bridge/cv_bridge.h"
#include "qrb_laser_cam_calibrator/calib_data.hpp"
#include "rclcpp/rclcpp.hpp"
#include "sensor_msgs/msg/compressed_image.hpp"
#include "sensor_msgs/msg/image.hpp"
#include "sensor_msgs/msg/laser_scan.hpp"
#include <opencv2/opencv.hpp>
namespace qrb_ros {
namespace calibrator_caller {
class DataCollector : public rclcpp::Node {
public:
  void start_capture();
  void capture_laser_plane_image();
  std::vector<qrb::laser_cam_calibrator::CameraData> camera_data_set;
  std::vector<qrb::laser_cam_calibrator::LaserData> laser_data_set;
  qrb::laser_cam_calibrator::LaserPlane laser_plane_image;
  bool current_capture_succeed();
  bool laser_plane_image_capture_succeed();
  DataCollector(const std::string &image_topic, const std::string &laser_topic);

private:
  bool image_captured_ = true;
  bool laser_captured_ = true;
  bool capture_laser_plane_image_ = false;
  int32_t last_laser_time_ = 0;
  int32_t last_image_time_ = 0;
  int32_t collected_frame_ = 0;
  pcl::PointCloud<pcl::PointXYZ> cumulative_point_cloud_;
  rclcpp::Subscription<sensor_msgs::msg::LaserScan>::SharedPtr laser_sub_{
      nullptr};
  rclcpp::Subscription<sensor_msgs::msg::Image>::SharedPtr image_sub_{nullptr};
  void laser_sub_callback(sensor_msgs::msg::LaserScan::ConstPtr laser_msg);
  void image_sub_callback(sensor_msgs::msg::Image::SharedPtr image_msg);
  void laser_msg2point_cloud(
      const sensor_msgs::msg::LaserScan::ConstSharedPtr scan_msg,
      pcl::PointCloud<pcl::PointXYZ> &src_cloud);
};
} // namespace calibrator_caller
} // namespace qrb_ros
#endif