/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef CALIBRATOR_CALLER_HPP_
#define CALIBRATOR_CALLER_HPP_
#include "laser_cam_calibra/data_collector.hpp"
#include "laser_cam_calibra/parameters_io.hpp"
#include "qrb_laser_cam_calibrator/calibrator.hpp"

namespace qrb_ros {
namespace calibrator_caller {
class CalibratorCaller {
private:
  std::shared_ptr<ParametersIO> parameters_io_ = nullptr;
  std::shared_ptr<DataCollector> capture_data_node_ = nullptr;
  std::shared_ptr<qrb::laser_cam_calibrator::Calibrator> calibrator_ = nullptr;
  double relative_dist_;
  void viz_test(std::vector<qrb::laser_cam_calibrator::CameraData> &cam_datas,
                std::vector<qrb::laser_cam_calibrator::LaserData> &laser_datas);
  void draw_line(cv::Mat &image, const Eigen::Vector3d &image_line);

public:
  CalibratorCaller();
  void capture_laser_plane_image();
  void save_all_data();
  void calibrate_with_saved_data();
  void capture_data();
  void calibrate();
  Eigen::Matrix3d R_l2c;
  Eigen::Vector3d t_l2c;
};
} // namespace calibrator_caller
} // namespace qrb_ros

#endif