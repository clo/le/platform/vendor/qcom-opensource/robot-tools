/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef CALIBRATOR_HPP_
#define CALIBRATOR_HPP_
#include "qrb_laser_cam_calibrator/calib_data.hpp"
#include "qrb_laser_cam_calibrator/data_processor.hpp"
#include "qrb_laser_cam_calibrator/parameters_input.hpp"
#include "qrb_laser_cam_calibrator/solver.hpp"
#include <eigen3/Eigen/Core>
namespace qrb {
namespace laser_cam_calibrator {
class Calibrator {
private:
  CameraInfo cam_info_;
  ChessboardInfo chessboard_info_;
  EnvParameters env_info_;
  LaserPlane laser_plane_;
  std::vector<std::string> laser_axis_;
  std::shared_ptr<Data_Processor> data_processor_ = nullptr;
  std::shared_ptr<Solver> solver_ = nullptr;

public:
  std::vector<CameraData> cam_data_set_;
  std::vector<LaserData> laser_data_set_;
  void initialize(const CameraInfo &cam_info,
                  const ChessboardInfo &chessboard_info,
                  const EnvParameters &env_info);
  void set_laser_plane(const LaserPlane &laser_plane);
  void set_laser_axis_(const std::vector<std::string> &laser_axis);
  bool find_chessboard(const cv::Mat &image);
  void input_data(const std::vector<CameraData> &cam_data_set,
                  const std::vector<LaserData> &laser_data_set);
  bool calibrate();
  Eigen::Matrix3d R_l2c;
  Eigen::Vector3d t_l2c;
};
} // namespace laser_cam_calibrator
} // namespace qrb

#endif