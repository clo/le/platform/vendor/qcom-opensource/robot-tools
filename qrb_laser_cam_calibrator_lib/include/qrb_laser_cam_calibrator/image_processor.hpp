/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef IMAGE_PROCESSOR_HPP_
#define IMAGE_PROCESSOR_HPP_
#include "qrb_laser_cam_calibrator/calib_data.hpp"
#include "qrb_laser_cam_calibrator/parameters_input.hpp"
#include <opencv2/opencv.hpp>
#include <vector>
namespace qrb {
namespace laser_cam_calibrator {
class ImageProcessor {
private:
  CameraInfo camera_info_;
  ChessboardInfo chessboard_info_;
  std::vector<cv::Point3f> world_pts_;
  cv::Size pattern_size_;
  cv::Mat intrinsic_;
  cv::Mat distortion_;

  void solve_pattern_pose(const std::vector<cv::Point2f> cornors, cv::Mat &rot,
                          cv::Mat &t);

public:
  void initialize(const CameraInfo &camera_info,
                  const ChessboardInfo &chessboard_info);
  bool find_chessboard(const cv::Mat &image);
  void get_target_pose(const cv::Mat &image, cv::Mat &rot, cv::Mat &t);
};
} // namespace laser_cam_calibrator
} // namespace qrb
#endif