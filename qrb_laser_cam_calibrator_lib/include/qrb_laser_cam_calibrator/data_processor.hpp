/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef DATA_PROCESSOR_HPP_
#define DATA_PROCESSOR_HPP_
#include "qrb_laser_cam_calibrator/image_processor.hpp"
#include "qrb_laser_cam_calibrator/laser_processor.hpp"
#include <opencv2/core/eigen.hpp>
namespace qrb {
namespace laser_cam_calibrator {
class Data_Processor {
private:
  std::shared_ptr<ImageProcessor> image_processor_ = nullptr;
  std::shared_ptr<LaserProcessor> laser_processor_ = nullptr;
  CameraInfo cam_info_;
  ChessboardInfo chessboard_info_;
  std::vector<double> dists_;
  void solve_margin_line(CameraData &cam_data);
  void line_fit(const Eigen::Vector3d &pt1, const Eigen::Vector3d &pt2,
                Eigen::Vector3d &line);

public:
  void set_cam_info(const CameraInfo &cam_info,
                    const ChessboardInfo &chessboard_info);
  void set_env_paramaters(const EnvParameters &env_prameters);
  bool find_chessboard(const cv::Mat &image);
  void solve_laser_plane_parameters(LaserPlane &laser_plane,
                                    Eigen::Vector4d &plane);
  void solve_chessboard_plane_parameters(CameraData &cam_data_set);
  void process_image_data(std::vector<CameraData> &cam_data_set);
  void process_laser_data(std::vector<LaserData> &laser_data_set);
};
} // namespace laser_cam_calibrator
} // namespace qrb
#endif