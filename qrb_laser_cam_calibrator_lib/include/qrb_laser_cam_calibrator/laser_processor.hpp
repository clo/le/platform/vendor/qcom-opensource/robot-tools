/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#ifndef LASER_PROCESSOR_HPP_
#define LASER_PROCESSOR_HPP_
#include <pcl/PCLHeader.h>
#include <pcl/common/common_headers.h>
#include <pcl/filters/extract_indices.h>
#include <pcl/point_types.h>
#include <pcl/segmentation/sac_segmentation.h>
#include "qrb_laser_cam_calibrator/calib_data.hpp"
#include <eigen3/Eigen/Core>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/io/pcd_io.h>
#include <pcl/kdtree/kdtree.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/registration/icp.h>
#include <pcl/registration/icp_nl.h>
#include <pcl/registration/transformation_estimation.h>
#include <pcl/registration/transformation_estimation_2D.h>
#include <pcl/registration/transformation_estimation_lm.h>
#include <pcl/search/kdtree.h>
namespace qrb {
namespace laser_cam_calibrator {
class LaserProcessor {
private:
  double max_dist_seen_as_continuous_;
  double ransac_fitline_dist_th_;
  int ransac_max_iterations_;
  int min_point_num_;
  double min_proportion_;
  double chessboard_length_in_laser_frame_;
  pcl::SACSegmentation<pcl::PointXYZ> seg_;
  pcl::ExtractIndices<pcl::PointXYZ> extractor_;
  void clustering(pcl::PointCloud<pcl::PointXYZ> &point_cloud,
                  std::vector<std::vector<int>> &cluster);
  void seg_all_lines(pcl::PointCloud<pcl::PointXYZ> &source_points,
                     std::vector<pcl::ModelCoefficients> &coefficients_set,
                     std::vector<pcl::PointCloud<pcl::PointXYZ>> &line_seg_set);
  void filter_using_distance(
      const std::vector<pcl::PointCloud<pcl::PointXYZ>> &line_seg_set,
      const double &distance, int &id);
  void filter_using_length(
      const std::vector<std::pair<pcl::PointXYZ, pcl::PointXYZ>> &end_pt_set,
      int &id);
  double dist(const pcl::PointXYZ &a, const pcl::PointXYZ &b);
  double mean_dist(const pcl::PointCloud<pcl::PointXYZ> &line_seg);
  void search_end_pts(const pcl::PointCloud<pcl::PointXYZ> &line_seg,
                      std::pair<pcl::PointXYZ, pcl::PointXYZ> &endpts);
  void clustered_point_clouds(
      const pcl::PointCloud<pcl::PointXYZ> &point_cloud,
      const std::vector<std::vector<int>> &cluster,
      std::vector<pcl::PointCloud<pcl::PointXYZ>> &all_clustered_sets);
  void calculate_parameter(const pcl::ModelCoefficients &fitted_line_parameter,
                           LaserData &laser_data);

public:
  void
  set_laser_process_parameters(const double &max_dist_seen_as_continuous,
                               const double &ransac_fitline_dist_th,
                               const int &ransac_max_iterations,
                               const int &min_point_num_stop_ransac,
                               const double &min_proportion_stop_ransac,
                               const double &chessboard_length_in_laser_frame);
  void process_laser_data(LaserData &laser_data, double &distance);
};
} // namespace laser_cam_calibrator
} // namespace qrb

#endif