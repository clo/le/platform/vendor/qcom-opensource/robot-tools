/*
 * Copyright (c) 2024 Qualcomm Innovation Center, Inc. All rights reserved.
 * SPDX-License-Identifier: BSD-3-Clause-Clear
 */
#include "qrb_laser_cam_calibrator/calibrator.hpp"
namespace qrb {
namespace laser_cam_calibrator {
void Calibrator::initialize(const CameraInfo &cam_info,
                            const ChessboardInfo &chessboard_info,
                            const EnvParameters &env_info) {
  cam_info_ = cam_info;
  chessboard_info_ = chessboard_info;
  env_info_ = env_info;
  data_processor_ = std::make_shared<Data_Processor>();
  data_processor_->set_cam_info(cam_info_, chessboard_info_);
  data_processor_->set_env_paramaters(env_info_);
}
bool Calibrator::find_chessboard(const cv::Mat &image) {
  return data_processor_->find_chessboard(image);
}
void Calibrator::set_laser_plane(const LaserPlane &laser_plane) {
  laser_plane_ = laser_plane;
}
void Calibrator::input_data(const std::vector<CameraData> &cam_data_set,
                            const std::vector<LaserData> &laser_data_set) {
  cam_data_set_ = cam_data_set;
  laser_data_set_ = laser_data_set;
}
void Calibrator::set_laser_axis_(const std::vector<std::string> &laser_axis) {
  laser_axis_ = laser_axis;
}
bool Calibrator::calibrate() {
  Eigen::Vector4d laser_plane;
  data_processor_->solve_laser_plane_parameters(laser_plane_, laser_plane);
  data_processor_->process_image_data(cam_data_set_);
  data_processor_->process_laser_data(laser_data_set_);
  solver_ =
      std::make_shared<Solver>(laser_data_set_, cam_data_set_, laser_plane);
  solver_->set_camera_info(cam_info_);
  solver_->set_first_orientation(laser_plane_.chessboard_orientation);
  bool done = solver_->solve_rotation(laser_axis_);
  if(!done){
    return false;
  }
  done = solver_->calibrate();
  if(!done){
    return false;
  }
  cam_data_set_ = solver_->cam_data_set_;
  laser_data_set_ = solver_->laser_data_set_;
  R_l2c = solver_->R_l2c;
  t_l2c = solver_->t_l2c;
  return true;
}
} // namespace laser_cam_calibrator
} // namespace qrb